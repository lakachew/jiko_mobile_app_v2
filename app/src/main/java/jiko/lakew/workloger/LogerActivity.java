package jiko.lakew.workloger;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import jiko.lakew.workloger.library.DBHandler;
import jiko.lakew.workloger.controllers.UserController;

public class LogerActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    //Definitions for the View
    public Button startButton;
    public Button stopButton;
    public EditText buildingNameEditText;
    public EditText descriptionEditText;
    public EditText companyNameEditText;
    public EditText addressEditText;
    public EditText houseNoEditText;
    public TextView durationTextView;

    //Definitions for calculating and displaying the time duration
    private long startTime  = 0L;
    private Handler durationHandler = new Handler();
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;

    //Definition for accessing Location or Google Service
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private GoogleApiClient mGoogleApiClient;
    protected Location location;
    public double latitude;
    public double longitude;

    /**
     * Static Server response Key names
     */
    private static String KEY_SUCCESS = "success";
    private static String KEY_ERROR_MSG = "error_msg";
    private static String KEY_ERROR = "error";
    private static String KEY_WORK_ID = "id";

    /*
        Data's needed from server
    */
    public int userID = 0;
    public int workID = 0;
    public String remember_token = "";

    //Definitions required for manupilation
    public boolean startWork;


    @Override
    protected void onStart() {
        super.onStart();
        if(mGoogleApiClient != null){
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop()
    {
        if(mGoogleApiClient.isConnected())
        {
            mGoogleApiClient.disconnect();
        }

        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loger);

        //initializing the View's
        startButton = (Button) findViewById(R.id.startButton);
        stopButton = (Button) findViewById(R.id.stopButton);
        buildingNameEditText = (EditText) findViewById(R.id.buildingName);
        houseNoEditText = (EditText) findViewById(R.id.houseNo);
        companyNameEditText = (EditText) findViewById(R.id.companyName);
        addressEditText = (EditText) findViewById(R.id.address);
        descriptionEditText = (EditText) findViewById(R.id.description);
        durationTextView = (TextView) findViewById(R.id.duration);

        //deactivating the startButton and stopButton
        startButton.setEnabled(false);
        stopButton.setEnabled(false);

        //Setting Listeners for the buttons
        startButton.setOnClickListener(buttonListener);
        stopButton.setOnClickListener(buttonListener);


        /*
                GOOGLE SERVICE FOR LOCATION
         */

        // Checking the availability of google service
        if(isGoogleServiceAvailable()){

            // start Google service
            buildGoogleService();

            startButton.setEnabled(true);

        }
    }

    /*
            Method for building the Google API
     */
    protected synchronized void buildGoogleService() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    /*
        Method for Checking if the device support Google Service
     */
    private boolean isGoogleServiceAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if(resultCode != ConnectionResult.SUCCESS){
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode)){
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }else {
                Toast.makeText(this, "This device is not supported for using Jiko Oy work application.", Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }

        //Toast.makeText(this, "Google Service is Available.", Toast.LENGTH_LONG).show();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_loger, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /***
     *      Button Listener
     */
    private View.OnClickListener buttonListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v) {

            Button button = (Button) v;

            if(button.getText().equals("Start"))
            {

                if(isLocationFound()){
                    startTime  = SystemClock.uptimeMillis();
                    durationHandler.postDelayed(updateTimerThread, 0);

                    // Starting to send the Data to the server
                    NetDataAsync();

                    button.setEnabled(false);
                    stopButton.setEnabled(true);
                    startWork = true;
                    companyNameEditText.setEnabled(false);
                    addressEditText.setEnabled(false);
                    buildingNameEditText.setEnabled(false);
                    houseNoEditText.setEnabled(false);
                    descriptionEditText.setEnabled(false);
                }

            }
            else if(button.getText().equals("Stop"))
            {
                if(isLocationFound()){
                    timeSwapBuff += timeInMilliseconds;
                    durationHandler.removeCallbacks(updateTimerThread);

                    // Starting to send the Data to the server
                    NetDataAsync();

                    startButton.setText("Start Another Work");
                    startButton.setEnabled(true);
                    stopButton.setEnabled(false);
                    startWork = false;
                    companyNameEditText.setEnabled(true);
                    addressEditText.setEnabled(true);
                    buildingNameEditText.setEnabled(true);
                    houseNoEditText.setEnabled(true);
                    descriptionEditText.setEnabled(true);


                }

            }
            else if (button.getText().equals("Start Another Work"))
            {
                if(isLocationFound()){
                    startTime  = 0L;
                    timeInMilliseconds = 0L;
                    timeSwapBuff = 0L;
                    updatedTime = 0L;

                    // Starting to send the Data to the server
                    NetDataAsync();

                    startTime  = SystemClock.uptimeMillis();
                    durationHandler.postDelayed(updateTimerThread, 0);

                    button.setEnabled(false);
                    stopButton.setEnabled(true);
                    startWork = true;
                    companyNameEditText.setEnabled(false);
                    addressEditText.setEnabled(false);
                    buildingNameEditText.setEnabled(false);
                    houseNoEditText.setEnabled(false);
                    descriptionEditText.setEnabled(false);
                }

            }

        }
    };


    /**
     * Get's and Set's the Latitude and Longitude of the last location
     */
    private boolean isLocationFound() {

        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(location != null){
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            Toast.makeText(this, "Your location is: " + latitude + "/" + longitude, Toast.LENGTH_LONG).show();
            return true;
        }

        Toast.makeText(this, "Your location could not be found please turn your GPS 'ON' and try again.", Toast.LENGTH_LONG).show();

        return false;
    }

    /***
     * Displaying the Time elapsed
     */
    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

            updatedTime = timeSwapBuff + timeInMilliseconds;

            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            secs = secs % 60;
            int milliseconds = (int) (updatedTime % 1000);

            durationTextView.setText("" + mins + ":"
                    + String.format("%02d", secs) + ":" + String.format("%03d", milliseconds));

            durationHandler.postDelayed(this, 0);

        }
    };

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    /***
     *
     *          SQLite
     *
     *
     *
     */


    /**
     * Method for initializing the connecting Class
     *
     */
    public void NetDataAsync() {
        new NetCheck().execute();
    }

    /**
     * Async Task to check whether internet connection is working.
     * if so it will initialize the Authentication class
     */
    private class NetCheck extends AsyncTask<String, String, Boolean> {
        private ProgressDialog nDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog = new ProgressDialog(LogerActivity.this);
            nDialog.setTitle("Checking Network");
            nDialog.setMessage("Loading..");
            nDialog.setIndeterminate(false);
            nDialog.setCancelable(true);
            nDialog.show();
        }

        /**
         * Gets current device state and checks for working internet connection by trying Google.
         */
        @Override
        protected Boolean doInBackground(String... args) {
            ConnectivityManager cm = (ConnectivityManager)  LogerActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()) {
                try {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                    urlc.setConnectTimeout(3000);
                    urlc.connect();
                    if (urlc.getResponseCode() == 200) {
                        return true;
                    }
                } catch (MalformedURLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean th) {

            if (th == true) {
                nDialog.dismiss();
                new ProcessLogin().execute();
            } else {
                nDialog.dismiss();
                Toast.makeText(LogerActivity.this.getApplicationContext(),
                        "Error in Network Connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Async Task to get and send data to My Sql database through JSON respone.
     */
    private class ProcessLogin extends AsyncTask<String, String, JSONObject> {


        private ProgressDialog pDialog;

        String buildingName, houseNo, description, address, companyName;

        DBHandler db = new DBHandler(getApplicationContext());

        @Override
        protected void onPreExecute() {


            super.onPreExecute();

            buildingName = buildingNameEditText.getText().toString();
            houseNo = houseNoEditText.getText().toString();
            description = descriptionEditText.getText().toString();
            address = addressEditText.getText().toString();
            companyName = companyNameEditText.getText().toString();

            pDialog = new ProgressDialog(LogerActivity.this);
            pDialog.setTitle("Contacting Servers");
            pDialog.setMessage("Logging in ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... args)
        {
            userID = db.getUserid();
            remember_token = db.getToken();

            UserController userController = new UserController();

            JSONObject json;

            if(startWork)
            {
                workID = 0;

                json = userController.sendStartWorkData(buildingName, description, houseNo, latitude,
                        longitude, address, companyName, userID, remember_token);

                Log.i("Server startWk response", json + "");

                return json;
            }

            json = userController.sendEndWorkData(workID, latitude, longitude);

            Log.i("Server endWk response", json + "");

            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            try {
                if (json.getString(KEY_SUCCESS) != null) {

                    String res = json.getString(KEY_SUCCESS);

                    if (Integer.parseInt(res) == 1)
                    {
                        pDialog.setTitle("Getting Data");
                        pDialog.setMessage("Loading User Space");

                        JSONObject json_work_data = json.getJSONObject("work_data");

                        //Adds the new work data ID from the server
                        workID = json_work_data.getInt(KEY_WORK_ID);

                        pDialog.dismiss();

                    } else {

                        pDialog.dismiss();
                        Log.d("Server response: ", json.getString(KEY_ERROR_MSG));
                        Toast.makeText(LogerActivity.this.getApplicationContext(),
                                "Error from Server: " + json.getString(KEY_ERROR_MSG), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}
