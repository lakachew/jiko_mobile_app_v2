package jiko.lakew.workloger.models;

/**
 * Created by lakachew on 19/06/2016.
 */
public class User {
    int id;
    String firstName;
    String lastName;
    String email;
    String privilege;
    String createAt;
    String token;

    private static User instance = new User();

    private User(){}

    public static User getInstance(){
        return instance;
    }


    /**
     * Instanciate the user resources
     *
     * @param id
     * @param firstName
     * @param lastName
     * @param email
     * @param privilege
     * @param createAt
     * @param token
     */
    public void setUser( int id, String firstName, String lastName,
                            String email,String privilege, String createAt, String token){

        this.setId(id);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setEmail(email);
        this.setPrivilege(privilege);
        this.setCreateAt(createAt);
        this.setToken(token);
    }

    public User getUser(){
        return this;
    }


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPrivilege() {
        return privilege;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
