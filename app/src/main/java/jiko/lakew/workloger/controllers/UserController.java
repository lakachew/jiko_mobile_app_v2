package jiko.lakew.workloger.controllers;

/**
 * Author : Lakew, Lakachew
 * Email  : Lakachew@gmail.com
 **/

import android.content.Context;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import jiko.lakew.workloger.library.DBHandler;
import jiko.lakew.workloger.library.JSONParser;
import jiko.lakew.workloger.library.JWTParser;
import jiko.lakew.workloger.models.User;


public class UserController {

    private static User user;
    private JSONParser jsonParser;
    private String jsonString;

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;
    }

    private static String getUserURL = "http://192.168.1.100:8000/api/v3/authenticate/user";
    private static String registerURL = "http://www.cc.puv.fi/~e1000671/jyrki/jiko/WorkHourCMS/services/login.php";
    private static String forpassURL = "http://www.cc.puv.fi/~e1000671/jyrki/jiko/WorkHourCMS/services/login.php";
    private static String chgpassURL = "http://www.cc.puv.fi/~e1000671/jyrki/jiko/WorkHourCMS/services/login.php";

    private static String workDataURL = "http://192.168.1.102:8000/api/v1/worklogs";

    private static String login_tag = "login";
    private static String register_tag = "register";
    private static String forpass_tag = "forpass";
    private static String chgpass_tag = "chgpass";
    private static String sendStartWorkData_tag = "startWork";
    private static String sendEndWorkData_tag = "endWork";

    private static String EMAIL_KEY = "email";



    // constructor
    public UserController(){
        this.user = User.getInstance();
        this.jsonParser = new JSONParser();
    }


    /**
     * Generates the user object content from the authorized jwtString
     *
     * @param loginController
     * @return User object
     */

    public User getUserObject(LoginController loginController)
    {
        String jwtString = loginController.getJwtString();

        String headerString = "";
        String payloadString = "";
        String jwtHeaderAndClaim = "";


        if (jwtString.contains("."))
        {
            String[] jwtArray = jwtString.split(Pattern.quote("."));

            headerString = jwtArray[0];
            payloadString = jwtArray[1];

            jwtHeaderAndClaim = headerString + "." + payloadString + ".";

            Log.d("Header and Claim", jwtHeaderAndClaim );
        }
        else {
            throw new IllegalArgumentException("jwtString: " + jwtString + "does not contain the character . ");
        }

        try {

            Claims claims = Jwts.parser().parseClaimsJwt(jwtHeaderAndClaim).getBody();

            int id = Integer.parseInt(claims.getId());
            user.setId(id);
            user.setEmail(claims.get("email").toString());
            user.setFirstName(claims.get("first_name").toString());
            user.setLastName(claims.get("last_name").toString());
            user.setPrivilege(claims.get("privilege").toString());
            user.setCreateAt(claims.get("created_at").toString());
            user.setToken(jwtString);

        }catch (Exception e){
            Log.e("Exception ", e.toString());
        }

        return user;

    }


    public JSONObject getUserCredential(String jwtString) {
        JSONObject jsonObject = null;

        String jsonString = jsonParser.getJsonString(getUserURL, jwtString);

        System.out.println("user as jsonString: " + jsonString);

        try {
            jsonObject = new JSONObject(jsonString);
            Log.d("JSON user object:", jsonObject.toString());

        } catch (JSONException e) {
            Log.e("JSON Object ", "Exception: " + e.toString());
        }

        return jsonObject;
    }














    /**
     * Function to Send Work Data
     **/
    public JSONObject sendStartWorkData(String buildingName, String description, String houseNo, double latitude,
                                        double longitude, String address, String companyName, int userId, String remember_token){
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", sendStartWorkData_tag));
        params.add(new BasicNameValuePair("buildingName", buildingName));
        params.add(new BasicNameValuePair("description", description));
        params.add(new BasicNameValuePair("houseNo", houseNo));
        params.add(new BasicNameValuePair("latitude", Double.toString(latitude)));
        params.add(new BasicNameValuePair("longitude", Double.toString(longitude)));
        params.add(new BasicNameValuePair("address", address));
        params.add(new BasicNameValuePair("company_name", companyName));
        params.add(new BasicNameValuePair("userId", Integer.toString(userId)));
        params.add(new BasicNameValuePair("remember_token", remember_token));
        JSONObject json = jsonParser.getJSONFromUrl(workDataURL, params);
        return json;
    }

    public JSONObject sendEndWorkData(int workID, double latitude, double longitude){
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", sendEndWorkData_tag));
        params.add(new BasicNameValuePair("work_id", Integer.toString(workID)));
        params.add(new BasicNameValuePair("latitude", Double.toString(latitude)));
        params.add(new BasicNameValuePair("longitude", Double.toString(longitude)));
        JSONObject json = jsonParser.getJSONFromUrl(workDataURL, params);
        return json;
    }

    /**
     * Function to change password
     **/
    public JSONObject chgPass(String newpas, String email){
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", chgpass_tag));

        params.add(new BasicNameValuePair("newpas", newpas));
        params.add(new BasicNameValuePair("email", email));
        JSONObject json = jsonParser.getJSONFromUrl(chgpassURL, params);
        return json;
    }

    /**
     * Function to reset the password
     **/
    public JSONObject forPass(String forgotpassword){
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", forpass_tag));
        params.add(new BasicNameValuePair("forgotpassword", forgotpassword));
        JSONObject json = jsonParser. getJSONFromUrl (forpassURL, params);
        return json;
    }

     /**
      * Function to  Register
      **/
    public JSONObject registerUser(String fname, String lname, String email, String uname, String password){
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", register_tag));
        params.add(new BasicNameValuePair("fname", fname));
        params.add(new BasicNameValuePair("lname", lname));
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("uname", uname));
        params.add(new BasicNameValuePair("password", password));
        JSONObject json = jsonParser.getJSONFromUrl(registerURL,params);
        return json;
    }

    /**
     * Function to logout user
     * Resets the temporary data stored in SQLite Database
     * */
    public boolean logoutUser(Context context){
        DBHandler db = new DBHandler(context);
        db.resetTables();
        return true;
    }


}

