package jiko.lakew.workloger.controllers;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import jiko.lakew.workloger.library.JWTParser;

/**
 * Created by lakachew on 19/06/2016.
 */
public class LoginController {



    private String jwtString;

    private static String LOGINURL = "http://192.168.1.100:8000/api/v3/authenticate";

    private JWTParser jwtParser;

    public LoginController() {
        this.jwtString = "";
        this.jwtParser = JWTParser.getInstance();
    }

    public String getJwtString() {
        return jwtString;
    }

    public void setJwtString(String jwtString) {
        this.jwtString = jwtString;
    }

    /**
     * Function to authentiacte email and password and get a JWT string
     *
     * @param email
     * @param password
     * @return jwtString
     */
    public boolean login(String email, String password)
    {
        boolean isAuthenticated = false;

        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("password", password));

        //Authenticating the user
        isAuthenticated = jwtParser.authenticate(LOGINURL,params);

        if(isAuthenticated)
        {
            this.jwtString = jwtParser.getJWTString();

        }

        return isAuthenticated;
    }
}
