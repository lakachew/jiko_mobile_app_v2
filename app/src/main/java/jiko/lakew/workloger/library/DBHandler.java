package jiko.lakew.workloger.library;

/**
 * Author : Lakew, Lakachew
 * Email  : Lakachew@gmail.com
 **/




import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

public class DBHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "Jiko_Oy_4";

    // Login table name
    private static final String TABLE_LOGIN = "employee_4";

    // Login Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_FIRSTNAME = "first_name";
    private static final String KEY_LASTNAME = "last_name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PHONE = "phone";
    private static final String KEY_TOKEN = "remember_token";
    private static final String KEY_PRIVILEGE = "privilege";
    private static final String KEY_CREATED_DATE = "created_at";


    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_LOGIN + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_FIRSTNAME + " TEXT,"
                + KEY_LASTNAME + " TEXT,"
                + KEY_EMAIL + " TEXT UNIQUE,"
                + KEY_PHONE + " TEXT,"
                + KEY_TOKEN + " TEXT,"
                + KEY_PRIVILEGE + " TEXT,"
                + KEY_CREATED_DATE + " TEXT" + ")";
        db.execSQL(CREATE_LOGIN_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOGIN);

        // Create tables again
        onCreate(db);
    }

    /**
     * Storing user details in database
     * */
    public void addUser(int id, String fname, String lname, String email, String phone, String privilege, String created_date) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, id); // ID
        values.put(KEY_FIRSTNAME, fname); // FirstName
        values.put(KEY_LASTNAME, lname); // LastName
        values.put(KEY_EMAIL, email); // Email
        values.put(KEY_PHONE, phone); // Phone
        values.put(KEY_PRIVILEGE, privilege); // Privilege level
        values.put(KEY_CREATED_DATE, created_date); // Created Date


        // Inserting Row
        db.insert(TABLE_LOGIN, null, values);
        db.close(); // Closing database connection
    }


    /**
     * Getting user data from database
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String,String> user = new HashMap<String,String>();
        String selectQuery = "SELECT  * FROM " + TABLE_LOGIN;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if(cursor.getCount() > 0){
            user.put(KEY_FIRSTNAME, cursor.getString(1));
            user.put(KEY_LASTNAME, cursor.getString(2));
            user.put(KEY_EMAIL, cursor.getString(3));
            user.put(KEY_PHONE, cursor.getString(4));
            user.put(KEY_TOKEN, cursor.getString(5));
            user.put(KEY_PRIVILEGE, cursor.getString(6));
            user.put(KEY_CREATED_DATE, cursor.getString(7));

        }
        cursor.close();
        db.close();
        // return user
        return user;
    }

    /**
     * Getting user ID from database
     * */
    public int getUserid()
    {
        Log.d("Error: ", "Server side error 2");
        String selectQuery = "SELECT " + KEY_ID + " FROM " + TABLE_LOGIN ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // Move to first row
        cursor.moveToFirst();

        int id = cursor.getInt(cursor.getColumnIndex(KEY_ID));

        cursor.close();
        db.close();

        return id;
    }

    /**
     * Getting token from database
     * */
    public String getToken()
    {
        Log.d("Error: ", "Server side error 2");
        String selectQuery = "SELECT " + KEY_TOKEN + " FROM " + TABLE_LOGIN ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // Move to first row
        cursor.moveToFirst();

        String token = cursor.getString(cursor.getColumnIndex(KEY_TOKEN));

        cursor.close();
        db.close();

        return token;
    }

    /**
     * Getting user login status
     * return true if rows are there in table
     * */
    public int getRowCount() {
        String countQuery = "SELECT  * FROM " + TABLE_LOGIN;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int rowCount = cursor.getCount();
        db.close();
        cursor.close();

        // return row count
        return rowCount;
    }


    /**
     * Re crate database
     * Delete all tables and create them again
     * */
    public void resetTables(){
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_LOGIN, null, null);
        db.close();
    }

}
