
package jiko.lakew.workloger.library;

import android.util.Base64;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.crypto.spec.SecretKeySpec;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SigningKeyResolver;
import io.jsonwebtoken.impl.crypto.MacProvider;

/**
 * Created by lakachew on 15/06/2016.
 */


public class JWTParser {

    private String jwtString;

    private static JWTParser instance = new JWTParser();

    private JWTParser(){
        this.jwtString = "";
    }

    public static JWTParser getInstance(){
        return instance;
    }

    public boolean authenticate(String url, List<NameValuePair> params)
    {
        boolean authenticated = false;

        InputStream is = null;

        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new UrlEncodedFormEntity(params));

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            System.out.println("Error Message from DB: " + is.toString());
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            System.out.println("Error Message from DB: " + is.toString());
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Error Message from DB: " + is.toString());
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            jwtString = sb.toString();
            jwtString = jwtString.substring(1,jwtString.length()-2);

            Log.d("JWT string Content: ", jwtString);

            authenticated = true;

        } catch (Exception e) {
            Log.e("Buffer Error", e.toString());
        }

        return authenticated;

    }

    public String getJWTString() {

        return jwtString;
    }
    /**
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     */

    static private Key key = null;

    /*public JWTParser(Key key) {
        this.key = key;
    }*/

    private String getUserCridentials(String jwtString) {

        String headerString = "";
        String payloadString = "";
        String signatureString = "";

        String userCridential = "";

        String jwtHeaderAndClaim = "";

        try {

            if (jwtString.contains("."))
            {
                String[] jwtArray = jwtString.split(Pattern.quote("."));

                Log.d("parsed JWT Header: ", jwtArray[0] );
                Log.d("parsed JWT payload: ", jwtArray[1] );
                Log.d("parsed JWT Signature: ", jwtArray[2] );

                jwtHeaderAndClaim = headerString + "." + payloadString + ".";

                Log.d("Header and Claim", jwtHeaderAndClaim );

            }
            else {
                throw new IllegalArgumentException("jwtString: " + jwtString + "does not contain the character . ");
            }

            try {

                Claims claims = Jwts.parser().parseClaimsJwt(jwtHeaderAndClaim).getBody();

                System.out.println("ID of the claims: " + claims.getId());
                System.out.println("Subject: " + claims.get("first_name"));
                System.out.println("Issuer: " + claims.getIssuer());
                System.out.println("Expiration: " + claims.getExpiration());
                System.out.println("Audience: " + claims.getAudience());
                System.out.println("Issued At: " + claims.getIssuedAt());

            }catch (Exception e){
                Log.e("Signature Exception", e.toString());
            }


        } catch (Exception e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        return userCridential;
    }

    public String jwtPractice()
    {
        /*********** Generating ***************/
        Key key = MacProvider.generateKey();

        //Building the header
        ///****/ Jwts.builder().setHeaderParam("kid", 121).build();


        //building the jwtString
        ///****/String jwtString = Jwts.builder().setSubject("email").setPayload("password").signWith(SignatureAlgorithm.HS512, key).compact();


        //******************* authenticating ************/
        ///****/if (!Jwts.parser().setSigningKey(key).parseClaimsJws(jwtString).getBody().getSubject().equals("Lakie"))
        ///****/{
            //through signatureException if signature validation fails.
        ///****/   throw new AssertionError();
        ///****/}


        //Key resolver
        //can inspect the JWS header and body (claim or String) before the JWS signiture is verified.
        ///****/SigningKeyResolver resolver = new MySigningKeyResolver();

        ///****/Jws<Claims> jws =Jwts.parser().setSigningKeyResolver(resolver).parseClaimsJws(compact);


        //********** Encoding to Base64 */
        ///****/Jwt jwt = Jwts.parser().setSigningKey(key).parse(compactJwt);




        return "";
    }

    public String createJWT(String id, String issuer, String subject, long ttlMillis){

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis= System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //Generating random key string
        Key key = MacProvider.generateKey(signatureAlgorithm);

        //we will sign our JWT with our generated key
        String base64 = Base64.encodeToString(key.getEncoded(), Base64.DEFAULT);
        byte[] signingKey = Base64.decode(base64, Base64.DEFAULT);


        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(id)
                .setIssuedAt(now)
                .setSubject(subject)
                .setIssuer(issuer)
                .signWith(signatureAlgorithm, signingKey);

        //if it has been specified, let's add the expiration
        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        return builder.compact();

    }

    private void parseJwt(String jwt)
    {
        String base64 = Base64.encodeToString(this.key.getEncoded(), Base64.DEFAULT);
        byte[] secretByte = Base64.decode(base64, Base64.DEFAULT);

        //This line will throw an exception if it is not a signed JWS (as expected)
        Claims claims = Jwts.parser()
                .setSigningKey(secretByte)
                .parseClaimsJws(jwt).getBody();
        System.out.println("ID: " + claims.getId());
        System.out.println("Subject: " + claims.getSubject());
        System.out.println("Issuer: " + claims.getIssuer());
        System.out.println("Expiration: " + claims.getExpiration());
    }

}
