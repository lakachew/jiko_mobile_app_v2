package jiko.lakew.workloger.library;



import android.content.Entity;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
//import org.apache.http.entity.ContentType;
import org.apache.http.entity.HttpEntityWrapper;
//import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//JSON WEB TOKEN LIBRARY
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.CompressionCodecResolver;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.JwtHandler;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.SigningKeyResolver;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.impl.DefaultJwtParser;
import io.jsonwebtoken.impl.crypto.MacProvider;

import java.net.URLEncoder;
import java.security.Key;
//import javax.xml.bind.DatatypeConverter;
import android.util.Base64;
import android.webkit.HttpAuthHandler;


//import com.google.gson.JsonArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.xml.datatype.DatatypeConstants;

import static android.R.attr.apiKey;
import static android.R.attr.key;

public class JSONParser {

    static String headerString = "";
    static String payloadString = "";
    static String signatureString = "";



    public String getJsonString(String url, String jwtString) {

        String jsonString = "";

        // Creating HTTP client and post
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        httpPost.setHeader("Authorization", "Bearer \\{" +  jwtString + "\\}");

        // Making HTTP Request
        try {
            HttpResponse response = httpClient.execute(httpPost);

            jsonString = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
            System.out.println("Http String content: " + jsonString);

        } catch (ClientProtocolException e) {
            // writing exception to log
            e.printStackTrace();

        } catch (IOException e) {
            // writing exception to log
            e.printStackTrace();
        }

        return jsonString;

    }


    /***
     *
     *
     * OLD METHOD
     *
     *
     *
     *
     *
     */

    public JSONObject getJSONFromUrl(String url, List<NameValuePair> params) {

        InputStream is = null;
        JSONObject jObj = null;
        String jsonString = "";

        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new UrlEncodedFormEntity(params));

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            System.out.println("Error Message from DB: " + is.toString());
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            System.out.println("Error Message from DB: " + is.toString());
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Error Message from DB: " + is.toString());
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            jsonString = sb.toString();

            Log.d("JSON string value: ", jsonString);

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        try {

            jObj = new JSONObject(jsonString);
            Log.d("JSON object content", jObj.toString());

        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        return jObj;

    }



}
