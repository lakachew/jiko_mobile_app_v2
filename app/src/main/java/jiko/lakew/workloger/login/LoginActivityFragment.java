package jiko.lakew.workloger.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import jiko.lakew.workloger.LogerActivity;
import jiko.lakew.workloger.R;
import jiko.lakew.workloger.controllers.LoginController;
import jiko.lakew.workloger.controllers.UserController;
import jiko.lakew.workloger.models.User;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoginActivityFragment extends Fragment {

    // Definitions for the view
    private View view;
    private Button signInButton;
    private TextView forgetPassword;
    private EditText inputEmail;
    private EditText inputPassword;

    public LoginActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login, container, false);

        //Initializing the view contents
        inputEmail = (EditText) view.findViewById(R.id.email);
        inputPassword = (EditText) view.findViewById(R.id.password);
        signInButton = (Button) view.findViewById(R.id.signInButton);
        forgetPassword = (TextView) view.findViewById(R.id.forgPaWord);

        // On Click Listener for the forgetPassword Text Button
        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Defining, initializing and forwarding to the Password Reset Activity
                Intent myIntent = new Intent(view.getContext(), PasswordResetActivity.class );
                startActivityForResult(myIntent,0);
            }
        });


        /**
         * Login button Click Listener
         **/
        signInButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                // if Statement: for checking if the input values have been properly inserted and show requirment if needed
                if ((!inputEmail.getText().toString().equals("")) && (!inputPassword.getText().toString().equals(""))) {
                    //Log.d("Debuging info: ", " Wass Up Lakie for cloud");
                    NetAsync(view);
                } else if ((!inputEmail.getText().toString().equals(""))) {
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Password is required", Toast.LENGTH_SHORT).show();
                } else if ((!inputPassword.getText().toString().equals(""))) {
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Email is required", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Email and Password are required", Toast.LENGTH_SHORT).show();
                }
            }
        });


        return view;
    }

    /**
     * Method for initializing the connecting Class
     * @param view
     */
    public void NetAsync(View view) {
        new NetCheck().execute();
    }

    /**
     * Async Task to check whether internet connection is working.
     * if so it will initialize the Authentication class
     */
    private class NetCheck extends AsyncTask<String, String, Boolean> {
        private ProgressDialog nDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog = new ProgressDialog(getActivity());
            nDialog.setTitle("Checking Network");
            nDialog.setMessage("Loading..");
            nDialog.setIndeterminate(false);
            nDialog.setCancelable(true);
            nDialog.show();
        }

        /**
         * Gets current device state and checks for working internet connection by trying Google.
         */
        @Override
        protected Boolean doInBackground(String... args) {
            //Log.d("Debuging info 01: ", " checking for internet");
            ConnectivityManager cm = (ConnectivityManager)  getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()) {
                try {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                    urlc.setConnectTimeout(3000);
                    urlc.connect();
                    if (urlc.getResponseCode() == 200) {
                        return true;
                    }
                } catch (MalformedURLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            //Log.d("Debuging info 02: ", " checking for internet");
            return false;
        }

        @Override
        protected void onPostExecute(Boolean th) {

            //Log.d("Debuging info: ", " Wass Up Lakie for cloud");

            if (th == true) {
                nDialog.dismiss();
                new ProcessLogin().execute();
            } else {
                nDialog.dismiss();
                Toast.makeText(getActivity().getApplicationContext(),
                        "Error in Network Connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Async Task to get and send data to My Sql database through JSON respone.
     */
    private class ProcessLogin extends AsyncTask<String, String, User> {


        private ProgressDialog pDialog;

        String email, password;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            inputEmail = (EditText) view.findViewById(R.id.email);
            inputPassword = (EditText) view.findViewById(R.id.password);

            email = inputEmail.getText().toString();
            password = inputPassword.getText().toString();

            pDialog = new ProgressDialog(getActivity());
            pDialog.setTitle("Authorizing");
            pDialog.setMessage(".........");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected User doInBackground(String... args) {
            LoginController loginController = new LoginController();

            User user = User.getInstance();

            //Log.d("Debuging info 1: ", "Just before trying to access the cloud");
            if(loginController.login(email, password))
            {
                UserController userController = new UserController();
                user = userController.getUserObject(loginController);
            }

            return user;
        }

        @Override
        protected void onPostExecute(User user)
        {
            if(user == null)
            {
                pDialog.dismiss();
                Toast.makeText(getActivity().getApplicationContext(),
                        "Incorrect username/password", Toast.LENGTH_SHORT).show();
            }

            Intent userPanel = new Intent(getActivity().getApplicationContext(), LogerActivity.class);
            userPanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pDialog.dismiss();
            startActivity(userPanel);

        }
    }


}
